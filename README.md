# TDD 体験会
テスト駆動開発の体験をする。

## 環境
* ASP.NET Core 2.1
* xUnit.net
* Visual Studio + ReSharper + dotCover or Rider


## テスト駆動開発
TDD をやるけど、テストファーストとか、テストコードは書かないといけないよね! とかそういったのは忘れてください。

NUnit などのテスティングフレームワークを使い始めたところから、TDD にいきつくまでに段階があると思ってます。

1. 第一段階
    * 今までのやり方に NUnit を突っ込むだけ
    * 全てテストコードを書こうとして苦しむ
    * この段階はほとんど効果がないばかりか、非効率な場合が多い
    * なぜかテストコードを書けば書くほど仕様変更に弱く
2. 第二段階
    * 第一段階でやり続けると、テストコードを書いた方がいいところと悪いところの区別がつくようになる
    * 効果があるところだけをテストコードを書く
    * プラマイゼロから、プラス方向に転じる
    * ここまで辿りつけないチームも多い
3. 第三段階
    * よりテストコードのカバー範囲を広げるために、テストコードを書く前提の設計をする
    * テストファーストはテストコードを先に書くって考えるんじゃなくて、テストを優先して考える感じで
    * ここまでできるとかなりプラス方向になる
4. 第四段階
    * テストコードを書く行為そのものを設計の一環としてやる
    * 試行錯誤の道具として単体テスト
    * 実はここで使える技術はかなりいろいろ使える

第四段階のメリットだけを本日は体験し、第一段階でくじけないようにすることと、使えるところはどんどん取り入れていけるようになることを目指す。


## TDD 体験
TDD は設計 -> 実装 -> テストの流れを高速で繰り返すことで行うって言われてるけど、やっていない人から見たら、**全てが同時**というのが多分しっくりくる。サイクルはあるけど、一サイクルがものによっては 30 秒もないので...

題材は書籍リファクタリングがを簡略化したのがモデル。

| 分類 | 金額
| ---- | ----
| 普通 | 二泊まで 200 円、それ以降は一泊 200 円
| 新作 | 一泊 300 円
| 子供向け | 三泊まで 150 円、それ以降は一泊 150 円

1. 一番簡単に書けそうなテストコードをひとつだけ書く
2. 失敗を確認
3. それを通すための最低限のコードを書く
4. 成功するまで 3 をがんばる
5. 1 からもう一度

境界値テストとかは意外と面倒なので程々でやめて、次に行く。


## 参考書籍
* [テスト駆動開発](http://t-wada.hatenablog.jp/entry/tddbook) - 最近再翻訳されたテスト駆動開発について書かれている書籍。今日のよりもより実戦に近い内容となっている。
* [新装版 リファクタリング](http://shop.ohmsha.co.jp/shopdetail/000000003881/) - 最初の方に、テストコードでガードしてリファクタリングをしていく例がある。本体験会の仕様はこれをベースに簡略化したもの。
* [レガシーコード改善ガイド](https://www.shoeisha.co.jp/book/detail/9784798116839) - テストコードのないレガシーコードを改善していくための方法がある網羅されている。コードの読み方から、どのように安全にテストを組み込んでいくかなど、より実戦的なノウハウが書かれている。
* [アジャイルソフトウェア開発の奥義 第二版](https://www.sbcr.jp/products/4797347784.html) - アジャイルで必要となる技術、単体テスト・デザインパターン・プログラム設計の原則などの解説書。変化に強いとはどういうことかの参考になる。
* [ThoughtWorks アンソロジー](https://www.oreilly.co.jp/books/9784873113890/) - ThoughtWorks 社の社員がそれぞれの立場で開発について書いてある書籍。その中の、オブジェクト指向エクササイズはオブジェクト指向プログラミングの基礎。
* [現場で役立つシステム設計の原則](https://gihyo.jp/book/2017/978-4-7741-9087-7) - DDD 界隈で有名な増田氏が執筆した、変化に強い開発の解説書。自身の経験をもとにわかりやすく書かれている