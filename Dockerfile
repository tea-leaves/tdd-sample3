FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY TddSample.Web/*.csproj ./TddSample.Web/
RUN dotnet restore

# copy everything else and build app
COPY TddSample.Web/. ./TddSample.Web/
WORKDIR /app/TddSample.Web
RUN dotnet publish -c Release -o out


FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/TddSample.Web/out ./
ENTRYPOINT ["dotnet", "TddSample.Web.dll"]

EXPOSE 80